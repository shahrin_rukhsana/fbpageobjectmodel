package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CreatNewAccountModel extends BaseModel {

	public CreatNewAccountModel(WebDriver driver) {
		super(driver);

	}


	// ----------------------Webelement----------------------------------//
	public WebElement getFirstName() {

		WebElement e= driver.findElement(By.xpath("//input[@name='firstname']"));
		return e;
	}

	public WebElement getLastName() {

		WebElement e= driver.findElement(By.xpath("//input[@name='lastname']"));
		return e;

	}

	public WebElement getEmail() {

		WebElement e= driver.findElement(By.xpath("//input[@name='reg_email__']"));
		return e;

	}
	
	public WebElement getPassWord() {

		WebElement e= driver.findElement(By.xpath("//div[contains(text(),'New password')]/following-sibling::input"));
		return e;

	}
	

	public WebElement getBirthMonth() {

		WebElement e= driver.findElement(By.xpath("//select[@id='month']"));
		return e;

	}
	
	public WebElement getBirthDay() {

		WebElement e= driver.findElement(By.xpath("//select[@id='day']"));
		return e;

	}
	
	public WebElement getBirthYear() {

		WebElement e= driver.findElement(By.xpath("//select[@id='year']"));
		return e;

	}
	
	public WebElement getMaleRadioButton() {

		WebElement e= driver.findElement(By.xpath("//label[contains(text(),'Male')]/following-sibling::input"));
		return e;

	}

}
