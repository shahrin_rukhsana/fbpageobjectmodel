package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.testng.Assert.assertEquals;

public class BaseModel {

	WebDriver driver;
	WebDriverWait wait;

	public BaseModel(WebDriver driver) {

		this.driver = driver;
		wait = new WebDriverWait(driver, 20);
	}

	
}
