package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomeModel extends BaseModel{
	
	
	public HomeModel(WebDriver driver) {
		
		super(driver);
	}
	
	
	public WebElement createNewAccountButton() {
		
		WebElement e= driver.findElement(By.xpath("//*[contains(text(),'Create new account')]"));
		return e;
	}

}
