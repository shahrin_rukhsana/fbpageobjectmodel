package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import models.CreatNewAccountModel;

public class CreatNewAccountPage extends CreatNewAccountModel {

	public CreatNewAccountPage(WebDriver driver) {
		super(driver);
	}

	public void typeFirstName(String firstName) {

		getFirstName().sendKeys(firstName);
	}

	public void typeLastName(String lastName) {

		getLastName().sendKeys(lastName);
	}

	public void typeEmail(String email) {

		getEmail().sendKeys(email);
	}

	public void typePassWord(String pass) {

		getPassWord().sendKeys(pass);
	}

	public void selectBirthMonth(String month) {

		Select select = new Select(getBirthMonth());
		select.selectByVisibleText(month);
	}
	
	public void selectBirthDay(String day) {

		Select select = new Select(getBirthDay());
		select.selectByVisibleText(day);
	}
	
	public void selectBirthYear(String year) {

		Select select = new Select(getBirthYear());
		select.selectByVisibleText(year);
	}
	
	public void clickMaleRadioButton() {
		
		getMaleRadioButton().click();
	}

}
