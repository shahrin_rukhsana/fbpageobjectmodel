package pages;

import org.openqa.selenium.WebDriver;

import models.HomeModel;

public class HomePage extends HomeModel{

	public HomePage(WebDriver driver) {
		
		super(driver);
	}
	
	public void verifyHomePageTitle(String title) {
		
	}
	
	public void clickCreateAccountButton() {
		
		createNewAccountButton().click();
	}

}
