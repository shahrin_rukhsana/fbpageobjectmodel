package tests;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import lib.ExcelReader;

public class CreatNewAccountPageTest extends BaseTest {
	
	@Test(dataProvider="fb")
	public void creatNewAccountPageTest(String firstName,String lastName,String email,
			String pass,String month,String day,String year) throws InterruptedException {
		getHomePage();
		hp.clickCreateAccountButton();
		getNewAccountPage();
		Thread.sleep(1000);
		cnap.typeFirstName(firstName);
		
		cnap.typeLastName(lastName);
		cnap.typeEmail(email);
		cnap.typePassWord(pass);
		cnap.selectBirthMonth(month);
		cnap.selectBirthDay(day);
		cnap.selectBirthYear(year);
		cnap.clickMaleRadioButton();
		
		
	}
	
	@DataProvider(name="fb")
	
	public Object[][]getDataLoader() throws IOException{
		
		Object[][] t;
		
		String fileName="data/fb.xlsx";
		String sheetName="fb";
		ExcelReader er = new ExcelReader(fileName, sheetName);
		t=er.excelToArray();
		return t;
		
	}

}
