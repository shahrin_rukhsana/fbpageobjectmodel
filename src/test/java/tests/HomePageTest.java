package tests;

import org.testng.annotations.Test;

public class HomePageTest extends BaseTest {
	
	@Test
	public void homePageTest() {
		getHomePage();
		hp.verifyHomePageTitle("Facebook - log in or sign up");
		hp.clickCreateAccountButton();
		
	}

}
