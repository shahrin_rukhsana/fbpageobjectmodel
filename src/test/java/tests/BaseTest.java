package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import pages.CreatNewAccountPage;
import pages.HomePage;

public class BaseTest {
	static WebDriver driver;
	static HomePage hp;
	static CreatNewAccountPage cnap;
	
	
	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://www.facebook.com/");
		
		//hp = new HomePage(driver);
		
	}
	
	public void getHomePage() {
		hp = new HomePage(driver);
	}
	
	public void getNewAccountPage() {
		cnap = new CreatNewAccountPage(driver);
	}
	
	

}
